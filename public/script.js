const users = [
{
uid: 1,
email: 'john@dev.com',
personalInfo: {
name: 'John',
address: {
line1: 'westwish st',
line2: 'washmasher',
city: 'wallas',
state: 'WX'}}
},
{
uid: 63,
email: 'a.abken@larobe.edu.au',
personalInfo: {
name: 'amin',
address: {
line1: 'Heidelberg',
line2: '',
city: 'Melbourne',
state: 'VIC'}}
},
{
uid: 45,
email: 'Linda.Paterson@gmail.com',
personalInfo: {
name: 'Linda',
address: {
line1: 'Cherry st',
line2: 'Kangaroo Point',
city: 'Brisbane',
state: 'QLD'}}
}
]

function returnUsers(){
  var users_arr = [];
  for (var obj of users){
    users_arr.push([obj.personalInfo.name, obj.email, obj.personalInfo.address.state])
  }
  return users_arr;
}